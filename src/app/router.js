// import external modules
import React, { Component, Suspense, lazy } from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import Spinner from "../components/spinner/spinner";

// import internal(own) modules
import MainLayoutRoutes from "../layouts/routes/mainRoutes";
import FullPageLayout from "../layouts/routes/fullpageRoutes";
import ErrorLayoutRoute from "../layouts/routes/errorRoutes";

// Main Layout
const LazyEcommerceDashboard = lazy(() => import("../views/dashboard/ecommerceDashboard"));
const Courier = lazy(() => import("../views/courier/courier"));
const Finance = lazy(() => import("../views/finance/finance"));
const Manifest = lazy(() => import("../views/manifest/manifest"));
const Report = lazy(() => import("../views/report/report"));
const Setting = lazy(() => import("../views/setting/setting"));
const Upload = lazy(() => import("../views/upload/upload"));
const Users = lazy(() => import("../views/users/users"));
const Kurs = lazy(() => import("../views/kurs/kurs"));
const Airport = lazy(() => import("../views/airport/airport"));
const Airlines = lazy(() => import("../views/airlines/airlines"));
const Hscode = lazy(() => import("../views/hscode/hscode"));


// Full Layout
const LazyLogin = lazy(() => import("../views/pages/login"));

// Error Pages
const LazyErrorPage = lazy(() => import("../views/pages/error"));

class Router extends Component {
   render() {
      return (
         // Set the directory path if you are deplying in sub-folder
         <BrowserRouter basename="/">
            <Switch>
               {/* Dashboard Views */}
                <MainLayoutRoutes
                    exact
                    path="/dashboard"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <LazyEcommerceDashboard {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/users"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Users {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/kurs"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Kurs {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/airlines"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Airlines {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/airport"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Airport {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/hscode"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Hscode {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/courier"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Courier {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/finance"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Finance {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/manifest"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Manifest {...matchprops} />
                        </Suspense>
                    )}
                />

                <MainLayoutRoutes
                    exact
                    path="/report"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Report {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/setting"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Setting {...matchprops} />
                        </Suspense>
                    )}
                />
                <MainLayoutRoutes
                    exact
                    path="/upload"
                    render={matchprops => (
                        <Suspense fallback={<Spinner />}>
                            <Upload {...matchprops} />
                        </Suspense>
                    )}
                />

               <ErrorLayoutRoute
                  exact
                  path="/pages/error"
                  render={matchprops => (
                     <Suspense fallback={<Spinner />}>
                        <LazyErrorPage {...matchprops} />
                     </Suspense>
                  )}
               />

               <ErrorLayoutRoute
                  render={matchprops => (
                     <Suspense fallback={<Spinner />}>
                        <LazyErrorPage {...matchprops} />
                     </Suspense>
                  )}
               />
            </Switch>
         </BrowserRouter>
      );
   }
}

export default Router;
