import React, { Component, Fragment } from "react";
import CustomTabs from "../../components/tabs/customTabs";
import axios from 'axios'
import ContentHeader from "../../components/contentHead/contentHeader";
import ContentSubHeader from "../../components/contentHead/contentSubHeader";
import { Dropdown,
   DropdownToggle,
   DropdownMenu,
   DropdownItem,
   Card,
   CardText,
   CardBody,
   CardTitle,
   Row,
   Col,
   Table,
   NavLink,
   TabContent,
   TabPane,
   NavItem,
   Nav,
   Button,
   FormGroup,
   Label,
   Input,
    Badge,
   Modal,
   ModalHeader, ModalBody, Form, ModalFooter,
} from "reactstrap";
import classnames from "classnames";
import Paging from "../../components/Paging";
import qs from "qs";
import "prismjs/themes/prism-okaidia.css";
import {CheckSquare} from "react-feather"; //Include CSS

class Users extends Component {
   constructor() {
      super();
      this.state = {
         dropdownOpen: false,
         activeTab: "1",
         dataUsers:[],
         pageCount: 0,
         currentPage: 0,
         isLoading: false,
         isModalShow: false,
         sSizeDropdownOpen: false,
         modalbayar: false
      };
   }
   toggle = tab => {
      if (this.state.activeTab !== tab) {
         this.setState({
            activeTab: tab
         });
      }
   };

   sSizeToggle = () => {
      this.setState(prevState => ({
         sSizeDropdownOpen: !prevState.sSizeDropdownOpen
      }));
   };

   toggledropdown = () => {
      this.setState({
         dropdownOpen: !this.state.dropdownOpen
      });
   };

   handlePageClick(data) {
      let selected = data.selected; // zero based index

      this.setState({ currentPage: selected }, () => {
         this.loadData();
      });
   }

   detpinjaman = e => {
      window.location = "detail-pinjaman";
   };

   togglebayar  = () => {
      this.setState({
         modalbayar: !this.state.modalbayar
      });
   }

   loadData() {

      this.setState({ isLoading: true });

      const params = qs.stringify({
         page: this.state.currentPage + 1
      });

      axios({
         method: "post",
         url:  "" + params
      })
          .then(response => {
             const result = response.data;
             if (result.success === true) {
                this.setState({

                   dataPinjaman: result.data.data,
                   pageCount: result.data.last_page,
                   isLoading: false
                });
             } else {
                this.setState({

                   isLoading: false });
             }
          })
          .catch(error => {
             console.log(error);
             this.setState({
                dataUsers:[
                   {
                      id:'1',
                      name_pjt:'PT ABCD Makmur',
                      npwp_pjt:'918208048092',
                      alamat_pjt:'JL Ciledug Raya',
                      izin_pjt:'SJ/02/2018',
                      total_user:'6',
                      status:'Active',
                   },

                ],isLoading: false });

          });
   }

   componentDidMount() {
      this.loadData();
   }
   render() {
      return (
          <Fragment>
             <ContentHeader>Users</ContentHeader>

             <Row>

                {this.state.isLoading ? (
                    "Memuat Data..."
                ) : (
                    <Col sm="12">

                       <Card>
                          <CardBody>


                             <Nav tabs>
                                <NavItem>
                                   <NavLink
                                       className={classnames({
                                          active: this.state.activeTab === "1"
                                       })}
                                       onClick={() => {
                                          this.toggle("1");
                                       }}
                                   >
                                      Company
                                   </NavLink>
                                </NavItem>
                                <NavItem>
                                   <NavLink
                                       className={classnames({
                                          active: this.state.activeTab === "2"
                                       })}
                                       onClick={() => {
                                          this.toggle("2");
                                       }}
                                   >
                                      Users
                                   </NavLink>
                                </NavItem>
                             </Nav>
                             <TabContent activeTab={this.state.activeTab}>
                                <TabPane tabId="1">
                                   <Row>
                                      <Col xl="6" lg="6" md="6" >
                                         <FormGroup >

                                            <Button color="primary">
                                               Add Company
                                            </Button>
                                         </FormGroup>
                                      </Col>
                                      <Col xl="6" lg="6" md="6"  className="float-right">
                                         <FormGroup  className="float-right" >
                                            <Input  type="text" id="roundinput" name="roundinput" className="round float-right" placeholder="Search" />
                                         </FormGroup>
                                      </Col>
                                   </Row>
                                   <Row>
                                      <Col sm="12">
                                         {this.state.dataUsers.length === 0 ? (
                                             "Data Kosong..."
                                         ) : (
                                             <Table hover>
                                                <thead>
                                                <tr>
                                                   <th>Company Name</th>
                                                   <th>NPWP</th>
                                                   <th>Alamat</th>
                                                   <th>No Izin</th>
                                                   <th>Total Users</th>
                                                   <th>Status</th>
                                                   <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {this.state.dataUsers.map(item => {
                                                   return (
                                                       <tr key={item.id}>
                                                          <th>{item.name_pjt}</th>
                                                          <td>{item.npwp_pjt}</td>
                                                          <td>{item.alamat_pjt}</td>
                                                          <td>{item.izin_pjt}</td>
                                                          <td>{item.total_user}</td>
                                                          <td><Badge color="success">Active</Badge></td>

                                                          <td>
                                                             <Dropdown
                                                                 group
                                                                 isOpen={this.state.sSizeDropdownOpen}
                                                                 size="sm"
                                                                 toggle={this.sSizeToggle}
                                                             >
                                                                <DropdownToggle caret>Actions</DropdownToggle>
                                                                <DropdownMenu right>
                                                                   <DropdownItem>Detail</DropdownItem>
                                                                   <DropdownItem>Edit</DropdownItem>
                                                                   <DropdownItem>Delete</DropdownItem>
                                                                </DropdownMenu>
                                                             </Dropdown>
                                                          </td>
                                                       </tr>
                                                   );
                                                })}
                                                </tbody>
                                             </Table>

                                         )}
                                       </Col>
                                   </Row>
                                </TabPane>
                                <TabPane tabId="2">
                                   <Row>
                                      <Col sm="6">
                                         <Card body>
                                            <CardTitle>Special Title Treatment</CardTitle>
                                            <CardText>
                                               With supporting text below as a natural lead-in to
                                               additional content.
                                            </CardText>
                                            <Button className="btn btn-success">Go somewhere</Button>
                                         </Card>
                                      </Col>
                                      <Col sm="6">
                                         <Card body>
                                            <CardTitle>Special Title Treatment</CardTitle>
                                            <CardText>
                                               With supporting text below as a natural lead-in to
                                               additional content.
                                            </CardText>
                                            <Button className="btn btn-danger">Go somewhere</Button>
                                         </Card>
                                      </Col>
                                   </Row>
                                </TabPane>
                             </TabContent>


                          </CardBody>
                       </Card>
                    </Col>
                )}
             </Row>

          </Fragment>
      );
   }
}

export default Users;
