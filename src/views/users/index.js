import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Lists from './users'
// import Edit from './EditPinjaman'
import Analytics from 'react-router-ga';
//
// const FormAdd = (props) => (
//     <Forms {...props} mode="Tambah" />
// )
//
// const FormEdit = (props) => (
//     <Forms {...props} mode="Edit" />
// )

class Users extends React.Component {

    render () {
        return (
            <div className="animated fadeIn">
                <div className="row">
                
                    <Analytics id="UA-123076872-5" debug>
                        <Switch>
                            <Route exact path={`${this.props.match.url}`} component={Lists} />
                            {/*<Route path={`${this.props.match.url}/add`} component={FormAdd} />*/}
                            {/*<Route path={`${this.props.match.url}/edit/:no_nasabah`} component={FormEdit} />*/}
                        </Switch>
                    </Analytics>

                </div>
            </div>
        )
    }
}

export default Users;
