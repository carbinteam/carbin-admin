// import external modules
import React, { Component } from "react";
import Alert from 'react-s-alert';
import qs from 'qs';
import axios from 'axios';
import { updateStatusLogin } from '../../actions/authAction';
import { BrowserRouter, Switch } from "react-router-dom";
import { NavLink } from "react-router-dom";
import {
   Row,
   Col,
   Input,
   Form,
   FormGroup,
   Button,
   Label,
   Card,
   CardBody,
   CardFooter
} from "reactstrap";
import logo from "../../assets/img/logo_footer.png";
import {login} from '../../components/UserFunctions'



class Login extends Component {

   constructor() {
      super();

      this.state = {
         email: '',
         password: '',
         user_status:'',
         error:'',
         platform: 'web',
         buttonIsLoading: false
      };

      this.handleLogin = this.handleLogin.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.fireApiLogin = this.fireApiLogin.bind(this);

   }

   async handleLogin() {

      this.fireApiLogin();

   }

   handleChange(event) {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;

      this.setState({
         [name]: value
      });
   }
   fireApiLogin() {

      this.setState({buttonIsLoading: true})

      axios({
         method: 'post',
         data: qs.stringify(this.state),
         url: 'http://127.0.0.1:8000/api/authenticate'
      })
          .then(response => {
             const result = response.data;

             if (! result.status) {
                Alert.error("Username/Password Salah !")
             }
             else {

                if (result.status) {

                   localStorage.CARBIN_IS_LOGIN = true;
                   localStorage.status = result.data.status;
                   localStorage.isActive = result.data.isActive;
                   localStorage.token = result.token;
                   localStorage.user_status = result.data.user_status;

                   this.props.dispatch(updateStatusLogin(true,
                       result.data.isActive,
                       result.token,
                       result.data.user_status,


                   ))
                   Alert.success("Selamat datang kembali..")
                }
                else {

                   Alert.error("Akun anda tidak aktif")
                }
             }

             this.setState({buttonIsLoading: false})
          })
          .catch(error => {
             console.log(error)
             this.setState({buttonIsLoading: false})
             Alert.error("Gagal terhubung ke server!")
          })
   }


   handleChecked = e => {
      this.setState(prevState => ({
         isChecked: !prevState.isChecked
      }));
   };

   render() {
      return (
         <div className="container">
            <Row className="full-height-vh">
               <Col xs="12" className="d-flex align-items-center justify-content-center">
                  <Card className="gradient-blueberry text-center width-400">
                     <CardBody>
                        <br></br>
                        <br></br>
                        <img src={logo} style={{width: "80%" }}  />
                        <br></br>
                        <br></br>
                        <br></br>
                        <Form className="pt-2">
                           <FormGroup>
                              <Col md="12">
                                 <Input
                                     type="text"
                                     className="form-control"
                                     name="email"
                                     id="email"
                                     value={this.state.email}
                                     onChange={this.handleChange}
                                     placeholder="Email"
                                     required
                                 />
                              </Col>
                           </FormGroup>

                           <FormGroup>
                              <Col md="12">
                                 <Input
                                     type="password"
                                     className="form-control"
                                     name="password"
                                     id="password"
                                     value={this.state.password}
                                     onChange={this.handleChange}
                                     placeholder="Password"
                                     required
                                 />
                              </Col>
                           </FormGroup>

                           <FormGroup>
                              <Row>
                                 <Col md="12">
                                    <div className="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3">
                                       <Input
                                           type="checkbox"
                                           className="custom-control-input"
                                           checked={this.state.isChecked}
                                           onChange={this.handleChecked}
                                           id="rememberme"
                                       />
                                    </div>
                                 </Col>
                              </Row>
                           </FormGroup>
                           <FormGroup>
                              <Col md="12">

                                 <Button   color="danger" block className="btn-primary btn-raised"
                                           onClick={this.handleLogin}
                                          >
                                    {this.state.buttonIsLoading ? 'Loading...' : 'Login'}
                                 </Button>



                              </Col>
                           </FormGroup>
                        </Form>

                     </CardBody>

                  </Card>
                  <br></br>

               </Col>
            </Row>
         </div>
      );
   }
}

export default Login;
