import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect'
import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper'

const locationHelper = locationHelperBuilder({})

export const userIsAuthenticated = connectedRouterRedirect({
    redirectPath: '/login',
    authenticatedSelector: (state, props) => {
        return state.authReducer.isLogin;
    }
});

export const userIsNotAuthenticated = connectedRouterRedirect({
    authenticatedSelector: (state, props) => {
        return ! state.authReducer.isLogin;
    },
    allowRedirectBack: false,
    redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || '/',
});