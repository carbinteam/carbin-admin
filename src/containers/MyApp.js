import React from 'react';

import Alert from 'react-s-alert'

import {
    HashRouter,
    Route,
    Switch
} from 'react-router-dom'

import store from '../store'
import { Provider } from 'react-redux'

import {
    userIsAuthenticated,
    userIsNotAuthenticated
} from '../auth.js'

// Views
import Login from '../views/pages/login'
import Register from '../views/pages/register'


import App from '../containers/App/App'

const MyApp = () => (
    <div>
        <Alert stack={{limit: 3, spacing: 10}} timeout={4000} position="bottom-right" effect="slide" />
        <Provider store={store}>
            <HashRouter>
                <Switch>
                    <Route exact path="/login" name="Login Page" component={userIsNotAuthenticated(Login)}/>
                    <Route exact path="/register" name="Register Page" component={Register}/>
                    <Route path="/" name="Home" component={userIsAuthenticated(App)} />
                </Switch>
            </HashRouter>
        </Provider>
    </div>
)

export default MyApp