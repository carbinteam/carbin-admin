// import external modules
import React, { Component } from "react";
import {
   FloatingMenu,
   MainButton,
   ChildButton,
} from 'react-floating-button-menu';
import {
   Home,
   Mail,
   MessageSquare,
   ChevronRight,
   Aperture,
   Box,
   Edit,
   Grid,
   Layers,
   Sliders,
   Map,
   BarChart2,
   Calendar,
   Copy,
   Book,
   CheckSquare,
   LifeBuoy,
   Users
} from "react-feather";
import * as Icon from "react-feather";

import { NavLink } from "react-router-dom";

// Styling
import "../../../../assets/scss/components/sidebar/sidemenu/sidemenu.scss";
// import internal(own) modules
import SideMenu from "../sidemenuHelper";
import { Tooltip } from 'reactstrap';
import { UncontrolledTooltip } from "reactstrap";
import Track from "rc-slider/es/common/Track";

class SideMenuContent extends React.Component {
   state = {
      isOpen: false,
   }

   render() {
      const classes = 'tooltip-inner'
      return (
         <SideMenu className="sidebar-content" toggleSidebarMenu={this.props.toggleSidebarMenu}>

            
            <SideMenu.MenuSingleItem>
               <NavLink to="/dashboard" activeclassname="active"  id="dashboard">
                  <i className="menu-icon">
                     <Home size={30} />
                  </i>
                  <UncontrolledTooltip
                      placement="right"
                      target="dashboard"
                  >
                     Dashboard
                  </UncontrolledTooltip>
               </NavLink>
            </SideMenu.MenuSingleItem>


            <SideMenu.MenuSingleItem>
               <NavLink to="/users" activeclassname="active"  id="users" >
                  <i className="menu-icon">
                     <Icon.Users size={30} />
                  </i>
                  <UncontrolledTooltip
                      placement="right"
                      target="users"
                  >
                     Users
                  </UncontrolledTooltip>
               </NavLink>
            </SideMenu.MenuSingleItem>

            <SideMenu.MenuSingleItem>
               <NavLink to="/kurs" activeclassname="active"  id="kurs" >
                  <i className="menu-icon">
                     <Icon.DollarSign size={30} />
                  </i>
                  <UncontrolledTooltip
                      placement="right"
                      target="kurs"
                  >
                     Kurs
                  </UncontrolledTooltip>
               </NavLink>
            </SideMenu.MenuSingleItem>

            <SideMenu.MenuSingleItem>
               <NavLink to="/airlines" activeclassname="active"  id="airlines" >
                  <i className="menu-icon">
                     <Icon.Navigation size={30} />
                  </i>
                  <UncontrolledTooltip
                      placement="right"
                      target="airlines"
                  >
                     Airlines
                  </UncontrolledTooltip>
               </NavLink>
            </SideMenu.MenuSingleItem>

            <SideMenu.MenuSingleItem>
               <NavLink to="/airport" activeclassname="active"  id="airport" >
                  <i className="menu-icon">
                     <Icon.MapPin size={30} />
                  </i>
                  <UncontrolledTooltip
                      placement="right"
                      target="airport"
                  >
                     Airport
                  </UncontrolledTooltip>
               </NavLink>
            </SideMenu.MenuSingleItem>

            <SideMenu.MenuSingleItem>
               <NavLink to="/hscode" activeclassname="active"  id="hscode" >
                  <i className="menu-icon">
                     <Icon.Bookmark size={30} />
                  </i>
                  <UncontrolledTooltip
                      placement="right"
                      target="hscode"
                  >
                     HSCODE
                  </UncontrolledTooltip>
               </NavLink>
            </SideMenu.MenuSingleItem>

            <SideMenu.MenuSingleItem>
               <NavLink to="/manifest" activeclassname="active" id="manifest">
                  <i className="menu-icon">
                     <Layers size={30} />
                  </i>
                  <UncontrolledTooltip
                      placement="right"
                      target="manifest"
                  >
                     Manifest
                  </UncontrolledTooltip>
               </NavLink>
            </SideMenu.MenuSingleItem>

            <SideMenu.MenuSingleItem>
            <NavLink to="/courier" activeclassname="active" id="courier">
               <i className="menu-icon">
                  <Icon.Truck size={30} />
               </i>
               <UncontrolledTooltip
                   placement="right"
                   target="courier"
               >
                  Courier
               </UncontrolledTooltip>
            </NavLink>
         </SideMenu.MenuSingleItem>

            <SideMenu.MenuSingleItem>
               <NavLink to="/finance" activeclassname="active" id="finance">
                  <i className="menu-icon">
                     <Icon.CreditCard size={30} />
                  </i>
                  <UncontrolledTooltip
                      placement="right"
                      target="finance"
                  >
                     Finance
                  </UncontrolledTooltip>
               </NavLink>
            </SideMenu.MenuSingleItem>

            <SideMenu.MenuSingleItem>
               <NavLink to="/report" activeclassname="active" id="report">
                  <i className="menu-icon">
                     <Icon.Calendar size={30} />
                  </i>
                  <UncontrolledTooltip
                      placement="right"
                      target="report"
                  >
                     Report
                  </UncontrolledTooltip>
               </NavLink>
            </SideMenu.MenuSingleItem>
         </SideMenu>



      );
   }
}

export default SideMenuContent;
