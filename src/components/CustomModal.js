import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import PropTypes from 'prop-types'

class CustomModal extends Component {

    constructor(props) {
        super(props);
        
        this.handleToggle = this.handleToggle.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleToggle() {
        this.props.toggleModal()
    }

    handleSubmit() {
        this.props.handleSubmit()
    }

    render() {
        return (
            <Modal isOpen={this.props.isShow} toggle={this.handleToggle} className="modal-primary">
                <ModalHeader toggle={this.handleToggle}>{this.props.modalHeaderText}</ModalHeader>
                <ModalBody>
                    {this.props.modalBodyText}
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.handleSubmit}>{this.props.modalSubmitText}</Button>{' '}
                    <Button color="secondary" onClick={this.handleToggle}>Batal</Button>
                </ModalFooter>
            </Modal>
        )
    }
}

CustomModal.propTypes = {
    modalHeaderText: PropTypes.string.isRequired,
    modalBodyText: PropTypes.string.isRequired,
    modalSubmitText: PropTypes.string.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,
} 

export default CustomModal