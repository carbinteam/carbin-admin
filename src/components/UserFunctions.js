import axios from 'axios'

export const login = user => {
    return axios
        .post('http://127.0.0.1:8000/api/authenticate',{
            email: user.email,
            password: user.password
        },
            {
                headers: {'Content-Type': 'application/json'}
            })
        .then(res=> {
            localStorage.setItem('token',res.data.token)
            console.log(res)
        })
        .catch(err=> {
            console.log(err)
        })
}


export const getHome = () => {
    return axios
        .get('http://127.0.0.1:8000/api/user',{
              headers: { Authorization: `Bearer ${localStorage.token}`}
            })
        .then(res=> {
            console.log(res)
            return res.data
        })
        .catch(err=> {
            console.log(err)
        })
}



export const getProfile = user => {
    return axios
        .get('api/profile',
            {
                headers: {Authorization: `Bearer $(localStorage.usertoken)`}
            })
        .then(res => {
            localStorage.setItem('usertoken', res.data.token)
            console.log(res)
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
}
