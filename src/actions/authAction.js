export const updateStatusLogin = (
    status,
    isActive,
    token,
    email,
    password,
    name,
    user_status

) => ({
    type : 'UPDATE_STATUS_LOGIN',
    status:status,
    isActive: isActive,
    token: token,
    email: email,
    password: password,
    name: name,
    user_status: user_status,

})