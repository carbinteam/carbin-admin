const authReducer = (
    state = {
        isLogin                              : localStorage.CARBIN_IS_LOGIN !== undefined && localStorage.CARBIN_IS_LOGIN === "true" ? true : false,
        isActive                         : localStorage.isActive || '',
        token                         : localStorage.token || '',
        status                         : localStorage.status || '',
        user_status                         : localStorage.user_status || ''

    },
    action
) => {
    switch (action.type) {        
        case 'UPDATE_STATUS_LOGIN':

            localStorage.WASKITA_IS_LOGIN       = action.status;
            localStorage.isActive            = action.isActive;
            localStorage.token            = action.token;
            localStorage.status            = action.status;
            localStorage.user_status            = action.user_status;



            return {
                ...state,
                isLogin          : action.status,
            status             : action.status,
            email             : action.email,
            password             : action.password,
            name         : action.name,
                user_status         : action.user_status,
            }
        default:
            return state;
    }
}

export default authReducer